package pcoa;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class GroupeTestFlo {

	@Test
	public void testAjout() {
		Formation f=new Formation("DUTInfo");
		f.ajouter("algo", 1);
		f.ajouter("pcoa",2);
		Groupe g=new Groupe(f);
		Formation f2=new Formation("DUTGEA");
		
		Etudiant e1=new Etudiant(new Identite("1111","Arya","Starck"));
		Etudiant e2=new Etudiant(new Identite("2222","Amelia","Pond"));
		e2.definirFormation(f);
		e1.definirFormation(f2);
		
		g.ajouter(e2);
		g.ajouter(e1);
		assertTrue(g.getEtudiants().size()==1);
		
	}
	
	
	@Test
	public void testSuprimer(){
		Formation f=new Formation("DUTInfo");
		f.ajouter("algo", 1);
		f.ajouter("pcoa",2);
		Groupe g=new Groupe(f);
		

		Etudiant e=new Etudiant(new Identite("2222","Amelia","Pond"));
		e.definirFormation(f);

	
		g.ajouter(e);
		assertTrue(g.getEtudiants().size()==1);
		g.supprimer(e);
		assertTrue(g.getEtudiants().size()==0);
	}
	@Test
	public void testMoy() throws NoteException, MatiereException, FormationException{
		Identite id1 = new  Identite("1111","Amelia","Pond");
		Etudiant e1 = new Etudiant(id1);
		Identite id2 = new  Identite("2222","Arya","Starck");
		Etudiant e2 = new Etudiant(id2);
		Formation f = new Formation("1");
		f.ajouter("algo", 1);
		f.ajouter("francais", 3);
		f.ajouter("anglais", 1);
		e1.definirFormation(f);
		e2.definirFormation(f);
		Groupe g = new Groupe(f);
		g.ajouter(e1);
		g.ajouter(e2);		
		e1.ajouterNote("anglais", 5);
		e1.ajouterNote("francais", 18);
		e2.ajouterNote("anglais", 14);
		
		double res = g.moyenneGroupe();
		assertEquals("moyenne groupe fausse", res, 14.375, 0.001);
		
		res = g.moyenneGroupe("anglais");
		assertEquals("moyenne groupe fausse", res, 9.5, 0.001);
		
		
	}
	
	@Test
	public void TestTri() throws NoteException, MatiereException, FormationException{
		Identite id1 = new  Identite("1","toto","tata");
		Etudiant etu1 = new Etudiant(id1);
		Identite id2 = new  Identite("2","toto2","tata2");
		Etudiant etu2 = new Etudiant(id2);
		Formation f = new Formation("1");
		f.ajouter("electrotechnique", 1);
		f.ajouter("fr", 1);
		f.ajouter("anglais", 1);
		etu1.definirFormation(f);
		etu2.definirFormation(f);
		Groupe g = new Groupe(f);
		g.ajouter(etu1);
		g.ajouter(etu2);
		
		etu1.ajouterNote("anglais", 5);
		etu1.ajouterNote("fr", 15);
		etu2.ajouterNote("anglais", 15);
		
		g.triParMerite();
		
		List<Etudiant> etudiants = g.getEtudiants();
		String res="";
		for(int i=0 ; i < etudiants.size() ; i++)
        {
	    	res+=(etudiants.get(i).getIdentite().getNom()+"  "+etudiants.get(i).getIdentite().getPrenom()+"\n");
        }
		assertEquals(res,"toto  tata\ntoto2  tata2\n");
	}

}


