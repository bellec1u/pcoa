package pcoa;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Etudiant {
	
	private HashMap<String,List<Double>> resultats;
	private Formation formation;
	private Identite identite;
	/**
	 * @param identite
	 */
	public Etudiant(Identite identite) {
		resultats=new HashMap<>();
		this.identite = identite;
	}

	public void definirFormation(Formation form){
		formation=form;
	}
	
	public void ajouterNote(String matiere,double note) throws NoteException, MatiereException{
		if(note<0||note>20){
			throw new NoteException();
		}
		if(!(formation.getMatieres().containsKey(matiere))){
			throw new MatiereException(matiere);
		}
		List<Double> l;
		if(!(resultats.containsKey(matiere))){
			l=new LinkedList<Double>();
		}else{
			l=resultats.get(matiere);
		}
		l.add(new Double(note));
		resultats.put(matiere, l);
		
	}
	
	
	public double moyenne(String matiere) throws MatiereException{
		if(!(resultats.containsKey(matiere))){
			throw new MatiereException(matiere);
		}
		double moy=0;
		int nb_note=0;
		List<Double> l=resultats.get(matiere);
		ListIterator<Double> it=l.listIterator();
		if(it.hasNext()==false){
			return -1;
		}
		while(it.hasNext()){
			moy+=it.next();
			nb_note++;
		}
		return (moy/nb_note);
	}
	
	public double moyenne() throws MatiereException, FormationException{
		
		double moy=0;
	
		double somme_coeff=0;
		double coeff=0;
	
		for (String matiere : resultats.keySet()) {
			if(moyenne(matiere)==-1){
				coeff=0;
			}else{
			
					coeff=formation.donnerCoefMatiere(matiere);
					
			}
			moy+=moyenne(matiere)*coeff;
			somme_coeff+=coeff;
			}
			
				
			
			
			
			
		
		
		return (moy/somme_coeff);
	}
	
		
	
	public HashMap<String, List<Double>> getResultats() {
		return resultats;
	}

	public Formation getFormation() {
		return formation;
	}

	public Identite getIdentite() {
		return identite;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Etudiant other = (Etudiant) obj;
		if (formation == null) {
			if (other.formation != null)
				return false;
		} else if (!formation.equals(other.formation))
			return false;
		if (identite == null) {
			if (other.identite != null)
				return false;
		} else if (!identite.equals(other.identite))
			return false;
		if (resultats == null) {
			if (other.resultats != null)
				return false;
		} else if (!resultats.equals(other.resultats))
			return false;
		return true;
	}

	

	
	
	
}
