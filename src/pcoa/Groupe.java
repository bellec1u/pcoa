package pcoa;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;




public class Groupe {
	public List<Etudiant> getEtudiants() {
		return etudiants;
	}

	public Formation getFormation() {
		return formation;
	}

	private List<Etudiant> etudiants;
	private Formation formation;
	
	
	public Groupe(Formation f){
		formation=f;
		etudiants=new LinkedList<Etudiant>();
	}
	
	public void ajouter(Etudiant etu){
		if (etu.getFormation().getIdFormation().equals(this.formation.getIdFormation())){
			this.etudiants.add(etu);
		}
	}



	public void supprimer(Etudiant etu){
		Iterator<Etudiant> iter = etudiants.iterator(); 
		while (iter.hasNext()) {
			if (etu.equals(iter.next())){
				iter.remove();
			}
		}
	}
	
	public double moyenneGroupe() throws MatiereException, FormationException{
		double res = 0;
		int nbEtu = 0;
		Iterator<Etudiant> iter = etudiants.iterator(); 
		while (iter.hasNext()) {
			nbEtu++;
			res = res + iter.next().moyenne();
		}
		res = res / nbEtu;
		return res;
	}
	
	public double moyenneGroupe(String mat) throws MatiereException, FormationException{
		double res = 0;
		int nbEtu = 0;
		Iterator<Etudiant> iter = etudiants.iterator(); 
		while (iter.hasNext()) {
			nbEtu++;
			res = res + iter.next().moyenne(mat);
		}
		if (nbEtu != 0){
			res = res / nbEtu;
		}
		return res;
	}
	
	public void triParMerite() throws MatiereException, FormationException{
		Collections.sort(etudiants,new ComparateurMoyenne());
	}
	
	public void trilpha(){
		Collections.sort(etudiants,new ComparateurAlpha());
	    
	   
	}
	
	
	
}
