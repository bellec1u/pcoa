package pcoa;

import static org.junit.Assert.*;

import org.junit.Test;

public class GroupeTestBellec {

	@Test
	public void testAjoutSupr() {
		Identite id1 = new  Identite("1","toto","tata");
		Etudiant etu1 = new Etudiant(id1);
		Identite id2 = new  Identite("2","toto2","tata2");
		Etudiant etu2 = new Etudiant(id2);
		Identite id3 = new  Identite("3","toto3","tata3");
		Etudiant etu3 = new Etudiant(id3);
		Formation f = new Formation("1");
		f.ajouter("electrotechnique", 1);
		f.ajouter("fr", 1);
		f.ajouter("anglais", 1);
		etu1.definirFormation(f);
		etu2.definirFormation(f);
		etu3.definirFormation(f);
		Groupe g = new Groupe(f);
		g.ajouter(etu1);
		assertEquals("ajout non fait",  g.getEtudiants().size(), 1);
		g.ajouter(etu2);
		assertEquals("ajout non fait",  g.getEtudiants().size(), 2);
		g.ajouter(etu3);
		assertEquals("ajout non fait",  g.getEtudiants().size(), 3);
		g.supprimer(etu3);
		assertEquals("supr non fait",  g.getEtudiants().size(), 2);
		g.supprimer(etu3);
		assertEquals("erreur",  g.getEtudiants().size(), 2);
		
	}

	@Test
	public void testMoy() throws NoteException, MatiereException, FormationException{
		Identite id1 = new  Identite("1","toto","tata");
		Etudiant etu1 = new Etudiant(id1);
		Identite id2 = new  Identite("2","toto2","tata2");
		Etudiant etu2 = new Etudiant(id2);
		Formation f = new Formation("1");
		f.ajouter("electrotechnique", 1);
		f.ajouter("fr", 1);
		f.ajouter("anglais", 1);
		etu1.definirFormation(f);
		etu2.definirFormation(f);
		Groupe g = new Groupe(f);
		g.ajouter(etu1);
		g.ajouter(etu2);
		
		etu1.ajouterNote("anglais", 5);
		etu1.ajouterNote("fr", 15);
		etu2.ajouterNote("anglais", 15);
		
		double res = g.moyenneGroupe();
		assertEquals("moyenne groupe fausse", res, 12.5, 0.1);
		
		res = g.moyenneGroupe("anglais");
		assertEquals("moyenne groupe fausse", res, 10, 0.1);

	}
	
	@Test
	public void testTri(){
		Identite id1 = new  Identite("1","b","a");
		Etudiant etu1 = new Etudiant(id1);
		Identite id2 = new  Identite("2","a","zfbdgerger");
		Etudiant etu2 = new Etudiant(id2);
		Identite id3 = new  Identite("3","b","fdsz");
		Etudiant etu3 = new Etudiant(id3);
		Formation f = new Formation("1");
		f.ajouter("electrotechnique", 1);
		f.ajouter("fr", 1);
		f.ajouter("anglais", 1);
		etu1.definirFormation(f);
		etu2.definirFormation(f);
		etu3.definirFormation(f);
		Groupe g = new Groupe(f);
		g.ajouter(etu1);
		g.ajouter(etu2);
		g.ajouter(etu3);
		
		g.trilpha();
		
		assertEquals("mauvaise personne", g.getEtudiants().get(0).getIdentite().getNom(), "a");
		
		assertEquals("mauvaise personne", g.getEtudiants().get(1).getIdentite().getNom(), "b");
		assertEquals("mauvaise personne", g.getEtudiants().get(1).getIdentite().getPrenom(), "a");
		
		assertEquals("mauvaise personne", g.getEtudiants().get(2).getIdentite().getNom(), "b");
		assertEquals("mauvaise personne", g.getEtudiants().get(2).getIdentite().getPrenom(), "fdsz");
		
		
	}

}
