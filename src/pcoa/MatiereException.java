package pcoa;

public class MatiereException extends Exception {
	
	private String matiere;
	
	public MatiereException(String mat){
		matiere=mat;
	}
	
	
	public void printStackTrace(){
		System.out.println(matiere+" indisponible pour cette formation");
	}
}
