package pcoa;

public class Identite {
	private  String NIP;
	private String nom;
	private String prenom;
	
	public Identite(String nip, String n, String p){
		this.NIP = nip;
		this.nom = n;
		this.prenom = p;
	}

	public String getNIP() {
		return NIP;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}
	
}
