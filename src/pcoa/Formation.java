package pcoa;

import java.util.HashMap;

public class Formation {
	/**
	 * Variable contenant le nom de la formation
	 */
	private String idFormation;
	public HashMap<String, Double> getMatieres() {
		return matieres;
	}

	/**
	 * table de hashage contenant la mati�re et de sont coeficiant 
	 */
	private HashMap<String,Double> matieres;
	
	/**
	 * constructeur de la class
	 * @param id : String
	 * @param matiere : String
	 * @param coef : Double
	 */
	public Formation ( String id){
		
		this.idFormation = id;
		this.matieres = new HashMap<String,Double>();
		
	}
	
	/**
	 * methode permetant d'ajouter une matiere et sont coeficiant
	 * @param key : String
	 * @param value : double
	 */
	public void ajouter (String key, double value){
		
		this.matieres.put(key, value);
		
	}
	/**
	 * methode permetant de suprimer une matiere et sont coeficiant
	 * @param key
	 */
	public void suprimer(String key){
		
		this.matieres.remove(key);
		
	}
	
	/**
	 * methode permetant de retourner le coefficiant d'une matiere dans la formation si elle existe
	 * @param key : String
	 * @return double
	 * @throws FormationException
	 */
	public double donnerCoefMatiere (String key) throws FormationException{
		
		double res;
		if (this.matieres.containsKey(key)){
			res = this.matieres.get(key);
			return res;
		} else {
			throw new FormationException();
		}
		
		
	}

	public String getIdFormation() {
		return idFormation;
	}
	
	
}
