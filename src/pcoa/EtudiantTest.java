package pcoa;

import static org.junit.Assert.*;

import org.junit.Test;

public class EtudiantTest {

	@Test
	public void testAjouterNote() throws NoteException, MatiereException {
		Identite id = new  Identite("1","toto","tata");
		Etudiant etu = new Etudiant(id);
		Formation f = new Formation("1");
		f.ajouter("electrotechnique", 1);
		f.ajouter("fr", 1);
		f.ajouter("anglais", 1);
		etu.definirFormation(f);
		
		boolean test = true;
		
		assertEquals("l'hashmap  n'est pas vide",etu.getResultats().size(), 0);
		
		etu.ajouterNote("electrotechnique", 15);
		assertEquals("l'ajout n'a pas ete fait",etu.getResultats().size(), 1);
		
		try{
			etu.ajouterNote("fr", -1);
			assertEquals("l'ajout a ete fait",etu.getResultats().size(), 1);
		} catch (NoteException e) {
			test = false;
		}
		assertFalse(test);
		
		test = true;
		try{
			etu.ajouterNote("anglais", 21);
			assertEquals("l'ajout a ete fait",etu.getResultats().size(), 1);
		} catch (NoteException e) {
			test = false;
		}
		assertFalse(test);
		
		test = true;
		try{
			etu.ajouterNote("gedrg", 2);
			assertEquals("l'ajout a ete fait",etu.getResultats().size(), 1);
		} catch (MatiereException e) {
			test = false;
		}
		assertFalse(test);
		
		etu.ajouterNote("anglais", 20);
		assertEquals("l'ajout n'a pas ete fait",etu.getResultats().size(), 2);
	}
	
	@Test
	public void testMoyenne() {
		Identite id = new  Identite("1","toto","tata");
		Etudiant etu = new Etudiant(id);
		Formation f = new Formation("1");
		f.ajouter("electrotechnique", 1);
		f.ajouter("fr", 1);
		f.ajouter("anglais", 1);
		etu.definirFormation(f);
		
		try {
			etu.ajouterNote("anglais", 5);
		} catch (NoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MatiereException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			etu.ajouterNote("anglais", 15);
		} catch (NoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MatiereException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		double res=0;
		try {
			res = etu.moyenne("anglais");
		} catch (MatiereException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals("la moyenne de l'angais n'a pas fonctionnee", res, 10, 0.1);
		
		try {
			etu.ajouterNote("fr", 20);
		} catch (NoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MatiereException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			etu.ajouterNote("fr", 10);
		} catch (NoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MatiereException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			try {
				res = etu.moyenne();
			} catch (FormationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (MatiereException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			res = etu.moyenne();
		} catch (MatiereException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FormationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertEquals("la moyenne generale n'a pas fonctionnee", res, 12.5, 0.1);
		
		
	}

	

}
