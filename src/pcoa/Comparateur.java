package pcoa;

public interface Comparateur {

	public boolean estMeilleur(Etudiant e1, Etudiant e2) throws MatiereException, FormationException;
}
