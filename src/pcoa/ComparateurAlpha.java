package pcoa;

import java.util.Comparator;

public class ComparateurAlpha implements Comparator<Etudiant> {

	@Override
	public int compare(Etudiant arg0, Etudiant arg1) {
		
		if( (arg0.getIdentite().getNom().compareTo(arg1.getIdentite().getNom())) > 0){
			
			return 1;
			
		}
		if( (arg0.getIdentite().getNom().compareTo(arg1.getIdentite().getNom())) == 0){
			if( (arg0.getIdentite().getPrenom().compareTo(arg1.getIdentite().getPrenom())) > 0){
				
				return 1;
			} else {
				return -1;
			}
		}
		return -1;
	}
}
